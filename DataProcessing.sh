#!/bin/sh
#Developed For use with OPTICON SCANNERS for Crescent Valley High School
#Copyleft 2012 All Wrongs Reserved (This Copyleft Notice must be maintained in derivative Works)
DATE=$(date +"%m-%d-%Y")
echo This will count the number of occurances in the input file
echo Copyleft 2012 You are free to copy and modify this script as you see fit
echo !--------------------------------------------------------------------------!
echo .				T-Shirt Winners                                .
echo !--------------------------------------------------------------------------!
echo T-Shirt Winners $DATE>> T-Shirt.txt
sort -f BarcodeData.TXT | uniq -c  | while read num word
do
	if [ 3 -eq $num ]
	then
		sort -f T-Shirt.txt | while read word2
		do
		if [ $word2 -eq $word ]
		then
		:
		else		
		echo $(echo $word|tr [a-z] [A-Z])  Won a T-Shirt
		echo $(echo $word|tr [a-z] [A-Z]) >> T-Shirt.txt
		fi
		done
	else
	:
	fi
done
echo "Press [Enter] to continue"
read "" -p
echo !---------------------------------------------------------------------------!
echo .				All Student Breakdown                           .
echo !---------------------------------------------------------------------------!
sort -f BarcodeData.TXT | uniq -c  | while read num word
do  
	echo $(echo $word|tr [a-z] [A-Z])  was present  $num times
done
